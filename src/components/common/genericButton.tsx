import React from "react";
import styled from "@emotion/styled";
import {useTheme} from "@mui/material";

type StyledGenericButtonProps = {
	[key: string]: any;
};

const StyledGenericButton = styled("div")<StyledGenericButtonProps>(() => {
	const theme = useTheme();
	return ({});
});

type GenericButtonProps = {
	[key: string]: any;
};

export const GenericButton: React.FC<GenericButtonProps> = ({...props}) => {
	return (
		<StyledGenericButton {...props}>

		</StyledGenericButton>
	);
};
