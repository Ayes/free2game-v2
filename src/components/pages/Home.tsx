import React from "react";
import styled from "@emotion/styled";
import {useTheme} from "@mui/material";

type StyledHomeProps = {
	[key: string]: any;
};

const StyledHome = styled("div")<StyledHomeProps>(() => {
	const theme = useTheme();
	return ({
		img: {
			position: "absolute",
			top: "0",
			left: 0
		}
	});
});

type HomeProps = {
	[key: string]: any;
};

export const Home: React.FC<HomeProps> = ({...props}) => {
	return (
		<StyledHome {...props}>
			<img
				src="https://einfachtierisch.de/media/cache/article_teaser/cms/2015/09/Graues-Kaetzchen-auf-Decke-shutterstock-dezi-134747402.jpg?522506"
				alt=""/>
		</StyledHome>
	);
};
