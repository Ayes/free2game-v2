import React from "react";
import styled from "@emotion/styled";
import {useTheme} from "@mui/material";

type StyledNavButtonProps = {
	[key: string]: any;
};

const StyledNavButton = styled("div")<StyledNavButtonProps>(() => {
	const theme = useTheme();
	return ({});
});

type NavButtonProps = {
	[key: string]: any;
};

export const NavButton: React.FC<NavButtonProps> = ({...props}) => {
	return (
		<StyledNavButton {...props}>

		</StyledNavButton>
	);
};
