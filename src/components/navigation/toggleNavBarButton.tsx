import styled from "@emotion/styled";
import {KeyboardDoubleArrowRight} from "@mui/icons-material";
import {IconButton, useTheme} from "@mui/material";
import React from "react";

type StyledToggleNavBarButtonProps = {
	[key: string]: any;
};

const StyledToggleNavBarButton = styled(IconButton)<StyledToggleNavBarButtonProps>(() => {
	const theme = useTheme();
	return ({
		display: "flex",
		placeItems: "center",
		padding: "8px",
		transition: "transform 500ms, inset 500ms, background 200ms",

		background: `${theme.palette.background.paper}dd`,
		filter: "drop-shadow(0 0 8px rgb(0, 0, 8px, 1))",
		border: `2px solid ${theme.palette.divider}`,
		backdropFilter: "blur(6px)",

		"&:hover": {
			background: `${theme.palette.background.paper}bc`
		},

		svg: {
			display: "block",
			aspectRatio: "inherit",
			width: "30px",
			height: "auto"
		}
	});
});

type ToggleNavBarButtonProps = {
	[key: string]: any;
};

export const ToggleNavBarButton: React.FC<ToggleNavBarButtonProps> = ({className, ...props}) => {
	return (
		<StyledToggleNavBarButton className={["toggleNavBar", className].join(" ")} {...props}>
			<KeyboardDoubleArrowRight/>
		</StyledToggleNavBarButton>
	);
};
