import {useQuery} from "@tanstack/react-query";
import React, {useState} from "react";
import styled from "@emotion/styled";
import {useTheme} from "@mui/material";
import {getHeroGif} from "../../api/requests";
import {Header} from "./Header";
import {NavBar} from "./NavBar";

type StyledLayoutProps = {
	[key: string]: any;
};

const StyledLayout = styled("div")<StyledLayoutProps>(({bIsExpanded}) => {

	const theme = useTheme();
	return ({
		display: "grid",
		gridTemplateColumns: `${bIsExpanded ? "250px" : "60px"} 1fr`,
		gridTemplateRows: `50px 1fr`,

		transition: "grid-template-columns 500ms",

		minHeight: "100vh",

		main: {
			"img, video, iframe": {
				width: "100%",
				maxHeight: "700px",
				objectFit: "cover",
				objectPosition: "center",
				display: "block"
			}
		}
	});
});
type LayoutProps = {
	children?: React.ReactNode;
	[key: string]: any;
};

export const Layout: React.FC<LayoutProps> = ({children, ...props}) => {
	const [bIsExpanded, bSetIsExpanded] = useState(false);
	return (
		<StyledLayout className="Layout" bIsExpanded={bIsExpanded} {...props}>

			<NavBar
				bIsExpanded={bIsExpanded}
				bSetIsExpanded={bSetIsExpanded}
				style={{gridRow: "1/3", gridColumn: "1/2"}}
			/>
			<Header style={{gridRow: "1/2", gridColumn: "2/3"}}/>

			<main style={{gridRow: "2/3", gridColumn: "2/3"}}>
				{children}
			</main>
		</StyledLayout>
	);
};
