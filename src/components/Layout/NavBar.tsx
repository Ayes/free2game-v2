import styled from "@emotion/styled";
import {Drafts, Inbox} from "@mui/icons-material";
import {
	ClickAwayListener,
	Divider,
	List,
	ListItem,
	ListItemButton,
	ListItemIcon,
	ListItemText,
	ListSubheader,
	Stack,
	useTheme
} from "@mui/material";
import React from "react";
import {ToggleNavBarButton} from "../navigation/toggleNavBarButton";

type StyledNavBarProps = {
	bIsExpanded: boolean;
	children?: React.ReactNode;
	[key: string]: any;
};

const StyledNavBar = styled("aside")<StyledNavBarProps>(({bIsExpanded}) => {
	const theme = useTheme();
	return ({
		display: "flex",
		position: "relative",
		backdropFilter: "blur(6px)",
		zIndex: "5",
		section: {
			borderRight: `2px solid ${theme.palette.divider}`,
			background: `${theme.palette.background.paper}dd`,

			overflowX: "hidden",
			padding: "8px",

			flex: "1 1 100%",

			display: "flex",
			flexDirection: "column",

			h3: {
				fontSize: "1.6rem",
				paddingInline: "50px",
				marginInline: "auto"
			}
		},
		"& > div": {
			position: "absolute",
			top: "42%",
			bottom: "42%",
			right: "-7rem",
			width: "7rem",
			overflow: "hidden",

			"&:hover": {
				".toggleNavBar": {
					right: "-2.5rem"
				}
			},
			".toggleNavBar": {
				transform: `translate(-50%, -50%) rotate(${bIsExpanded ? "180" : "0"}deg)`,

				top: "50%",
				right: "0"
			}
		}

	});
});

type NavBarProps = {
	bIsExpanded: boolean;
	bSetIsExpanded: (arg: boolean) => void;
	[key: string]: any;
};

export const NavBar: React.FC<NavBarProps> = ({bIsExpanded, bSetIsExpanded, ...props}) => {

	const toggleMenuState = (newState: boolean = !bIsExpanded) => bSetIsExpanded(newState);

	return (
		<ClickAwayListener onClickAway={() => toggleMenuState(false)}>
			<StyledNavBar
				bIsExpanded={bIsExpanded}
				elevation={3}
				{...props}
			>
				<section>
					<h3>
						Navigation
					</h3>
					<Stack
						direction="column"
						divider={<Divider orientation="horizontal"/>}
						spacing={2}
						useFlexGap
					>
					</Stack>
				</section>
				<div>
					<ToggleNavBarButton
						aria-label={"toggle menu state"}
						onClick={() => {
							toggleMenuState();
						}}
					/>
				</div>
			</StyledNavBar>
		</ClickAwayListener>

	);
};
