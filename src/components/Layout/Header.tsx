import React from "react";
import styled from "@emotion/styled";
import {AppBar, Paper, useTheme} from "@mui/material";

type StyledHeaderProps = {
	[key: string]: any;
};

const StyledHeader = styled(AppBar)<StyledHeaderProps>(() => {
	const theme = useTheme();
	return ({
		textAlign: "center",
		background: `${theme.palette.background.paper}dd`,
		borderBottom: `2px solid ${theme.palette.divider}`,
		zIndex: 5,
		backdropFilter: "blur(6px)"
	});
});

type HeaderProps = {
	[key: string]: any;
};

export const Header: React.FC<HeaderProps> = ({...props}) => {
	return (
		<StyledHeader {...props} position="sticky">
			<hgroup>
				<h2>
					GameDiscovery
				</h2>
				<h4>
					Discover the newest games!
				</h4>
			</hgroup>
			<nav>

			</nav>
		</StyledHeader>
	);
};
