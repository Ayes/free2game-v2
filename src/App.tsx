import {createTheme, CssBaseline, ThemeProvider} from "@mui/material";
import {useQuery} from "@tanstack/react-query";
import {createContext, useMemo, useState} from "react";
import {Route, Routes} from "react-router-dom";
import {Layout} from "./components/Layout/Layout";
import {getGames} from "./api/requests";
import {Home} from "./components/pages/Home";

const GameContext = createContext({});


const currentTheme = createTheme({
	palette: {
		mode: "dark"
	}
});

const App = () => {
	const games = useQuery({queryKey: ["games"], queryFn: getGames, staleTime: 1000 * 60}).data;

	return (
		<ThemeProvider theme={currentTheme}>
			<CssBaseline/>
			<div className="App">
				<Layout>
					<GameContext.Provider value={games}>
						<Routes>
							<Route path="/" element={<Home/>}/>
						</Routes>
					</GameContext.Provider>
				</Layout>
			</div>
		</ThemeProvider>
	);
};

export default App;
