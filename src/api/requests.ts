export const getGames = () => fetch(`https://api.rawg.io/api/games?key=${import.meta.env.VITE_RAWG_KEY}`)
	.then(response => response.json())
	.then(json => json);
export const getHeroGif = () => fetch(`https://tenor.googleapis.com/v2/search?key=${import.meta.env.VITE_TENOR_KEY}&q=game+trailer&random=true&limit=1`)
	.then(response => response.json())
	.then(json => json);
